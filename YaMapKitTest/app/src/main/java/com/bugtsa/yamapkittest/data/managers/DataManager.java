package com.bugtsa.yamapkittest.data.managers;


import com.bugtsa.yamapkittest.data.storage.dto.BalloonDto;

import java.util.ArrayList;
import java.util.List;

import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class DataManager {

    private List<BalloonDto> mBalloonList = new ArrayList<>();
    private List<OverlayItem> mOverlayItemList = new ArrayList<>();

    public static class DataManagerHolder {
        private static DataManager ourInstance = new DataManager();
    }

    public static DataManager getInstance() {
        return DataManagerHolder.ourInstance;
    }

    //region ================= BalloonList =================

    public List<BalloonDto> getBalloonList() {
        return mBalloonList;
    }

    public int getSizeBalloonList() {
        return mBalloonList.size();
    }

    public void addBalloonDto(BalloonDto balloonDto) {
       mBalloonList.add(balloonDto);
    }

    //endregion

    //region ================= OverlayItem =================

    public List<OverlayItem> getOverlayItemList() {
        return mOverlayItemList;
    }

    public void addOverlayItem(OverlayItem overlayItem) {
        mOverlayItemList.add(overlayItem);
    }

    //endregion
}
