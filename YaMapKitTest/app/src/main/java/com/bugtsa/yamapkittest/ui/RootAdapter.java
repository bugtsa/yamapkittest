package com.bugtsa.yamapkittest.ui;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bugtsa.yamapkittest.data.storage.dto.BalloonDto;
import com.bugtsa.yamapkittest.databinding.ItemBalloonBinding;

import java.util.ArrayList;
import java.util.List;

public class RootAdapter extends RecyclerView.Adapter<RootAdapter.ItemBalloonViewHolder> {

    private List<BalloonDto> mBalloonList = new ArrayList<>();

    @Override
    public ItemBalloonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemBalloonViewHolder(ItemBalloonBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false)
                .getRoot());
    }

    @Override
    public void onBindViewHolder(ItemBalloonViewHolder holder, int position) {
        BalloonDto balloonDto = mBalloonList.get(position);

        holder.mItemBalloonBinding.balloonIdTv.setText(String.valueOf(balloonDto.getId()));
        holder.mItemBalloonBinding.balloonTextTv.setText(balloonDto.getAddress());
    }

    @Override
    public int getItemCount() {
        return mBalloonList.size();
    }

    public void addBalloon(BalloonDto balloonDto) {
        mBalloonList.add(balloonDto);
        notifyItemChanged(mBalloonList.size() - 1);
    }

    public void addBalloonList(List<BalloonDto> balloonList) {
        mBalloonList.clear();
        mBalloonList.addAll(balloonList);
        notifyDataSetChanged();
    }

    class ItemBalloonViewHolder extends RecyclerView.ViewHolder {

        ItemBalloonBinding mItemBalloonBinding;

        public ItemBalloonViewHolder(View itemView) {
            super(itemView);
            mItemBalloonBinding = DataBindingUtil.bind(itemView);
        }
    }
}
