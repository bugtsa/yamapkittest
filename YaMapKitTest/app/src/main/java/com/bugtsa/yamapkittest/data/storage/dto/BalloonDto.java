package com.bugtsa.yamapkittest.data.storage.dto;


public class BalloonDto {

    private long id;
    private String address;
    private String comment;

    public BalloonDto(long id, String address, String comment) {
        this.id = id;
        this.address = address;
        this.comment = comment;
    }

    //region ================= Getters =================

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getComment() {
        return comment;
    }


    //endregion
}
