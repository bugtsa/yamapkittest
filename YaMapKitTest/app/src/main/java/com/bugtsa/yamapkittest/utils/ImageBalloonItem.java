package com.bugtsa.yamapkittest.utils;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bugtsa.yamapkittest.R;

import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class ImageBalloonItem extends BalloonItem implements OnBalloonListener {

    protected TextView imageView;
    protected TextView mAddress;
    Context mContext;

    public ImageBalloonItem(Context context, GeoPoint geoPoint) {
        super(context, geoPoint);
        mContext = context;
    }

    @Override
    public void inflateView(Context context){

        LayoutInflater inflater = LayoutInflater.from( context );
        model = (ViewGroup)inflater.inflate(R.layout.balloon_image_layout, null);
        imageView = (TextView)model.findViewById( R.id.balloon_comment_tv );
        mAddress = (TextView)model.findViewById( R.id.balloon_address_tv );
        setText(mAddress.getText());
    }

//    public void setImageId(int imageId){
//        if (imageView != null){
//            imageView.setImageResource( imageId );
//            setChange(true);
//        }
//    }
//
//    public void setImageBitmap(Bitmap bitmap) {
//        if (imageView != null){
//            imageView.setImageBitmap(bitmap);
//            setChange(true);
//        }
//    }

    public void setOnViewClickListener(){
//        setOnBalloonViewClickListener(R.id.balloon_image_view1, this);
//        setOnBalloonViewClickListener(R.id.balloon_text_view1, this);
    }

    @Override
    public void onBalloonViewClick(BalloonItem item, View view) {
        // TODO Auto-generated method stub
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        switch (view.getId()) {
            case R.id.balloon_comment_tv:
                dialog.setTitle("Click image");
                break;
            case R.id.balloon_address_tv:
                dialog.setTitle("Click text");
                break;
        }
        dialog.show();

    }

    @Override
    public void onBalloonShow(BalloonItem balloonItem) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBalloonHide(BalloonItem balloonItem) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBalloonAnimationStart(BalloonItem balloonItem) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBalloonAnimationEnd(BalloonItem balloonItem) {
        // TODO Auto-generated method stub

    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
