package com.bugtsa.yamapkittest.ui;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

import com.bugtsa.yamapkittest.R;
import com.bugtsa.yamapkittest.data.managers.DataManager;
import com.bugtsa.yamapkittest.data.storage.dto.BalloonDto;
import com.bugtsa.yamapkittest.databinding.ActivityRootBinding;
import com.bugtsa.yamapkittest.utils.Coordinate;
import com.bugtsa.yamapkittest.utils.ImageBalloonItem;

import java.util.List;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropOverlay;
import ru.yandex.yandexmapkit.overlay.location.MyLocationItem;
import ru.yandex.yandexmapkit.overlay.location.OnMyLocationListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class RootActivity extends Activity implements OnMyLocationListener {

    MapController mMapController;
    Resources mResources;
    private OverlayItem mTapItem;
    OverlayManager mOverlayManager;
    private customOverlay mOverlay;
    private Coordinate mX;
    private Coordinate mY;

    private static final int PERMISSIONS_CODE = 109;

    ActivityRootBinding mBinding;
    RootAdapter mRootAdapter;

    private DataManager mDataManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.bugtsa);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_root);

        mBinding.map.showBuiltInScreenButtons(true);
        mResources = getResources();
        mMapController = mBinding.map.getMapController();
        mOverlayManager = mMapController.getOverlayManager();
        mMapController.setZoomCurrent(10);
        // создание своего слоя
        mOverlay = new customOverlay(mMapController);
        mOverlayManager.addOverlay(mOverlay);

        // add listener
        mMapController.getOverlayManager().getMyLocation().addMyLocationListener(this);

        mDataManager = DataManager.getInstance();

        setupAdapter();

        checkPermission();
    }

    private void setupAdapter() {
        mBinding.balloonList.setLayoutManager(new LinearLayoutManager(this));
        mRootAdapter = new RootAdapter();
        mBinding.balloonList.setAdapter(mRootAdapter);
        if (mDataManager.getSizeBalloonList() != 0) {
            mRootAdapter.addBalloonList(mDataManager.getBalloonList());
            showOverlayItemList(mDataManager.getOverlayItemList());
        }
    }

    private void showOverlayItemList(List<OverlayItem> overlayItemList) {
        for (OverlayItem overlayItem : overlayItemList) {
            mOverlay.addOverlayItem(overlayItem);
        }
    }

    private void checkPermission() {
        int permACL = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permAFL = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permACL != PackageManager.PERMISSION_GRANTED ||
                permAFL != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_CODE);
        }

    }

    @Override
    public void onMyLocationChange(MyLocationItem myLocationItem) {
        // TODO Auto-generated method stub
        final TextView textView = new TextView(this);
        textView.setText("Type " + myLocationItem.getType() + " GeoPoint [" + myLocationItem.getGeoPoint().getLat() + "," + myLocationItem.getGeoPoint().getLon() + "]");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                mBinding.map.addView(textView);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMapController.getOverlayManager().getMyLocation().refreshPermission();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class customOverlay extends DragAndDropOverlay {

        public customOverlay(MapController arg0) {
            super(arg0);
        }

        @Override
        public boolean onSingleTapUp(float arg0, float arg1) {
            // получение координат экрана в рамках вьювера карты
            ScreenPoint s = new ScreenPoint(arg0, arg1);
            // преобразование экранных координат в географические
            GeoPoint g = mMapController.getGeoPoint(s);
            // создание и добавление новой метки(тайла)
            mTapItem = new DragAndDropItem(g, mResources.getDrawable(R.drawable.shop));
            ImageBalloonItem imageBalloon = new ImageBalloonItem(getApplicationContext(), mTapItem.getGeoPoint());
            imageBalloon.setOnViewClickListener();
            mTapItem.setBalloonItem(imageBalloon);

            mOverlay.addOverlayItem(mTapItem);
            mMapController.setPositionNoAnimationTo(g);

            saveItem();
            return super.onSingleTapUp(arg0, arg1);
        }
    }

    private void saveItem() {
        mX = new Coordinate(mTapItem.getGeoPoint().getLon());
        mY = new Coordinate(mTapItem.getGeoPoint().getLat());
        BalloonDto balloonDto = new BalloonDto(mDataManager.getSizeBalloonList(), mX.toString() + " " + mY.toString(), "");
        mDataManager.addBalloonDto(balloonDto);
        mRootAdapter.addBalloon(balloonDto);
        mDataManager.addOverlayItem(mTapItem);
    }
}
